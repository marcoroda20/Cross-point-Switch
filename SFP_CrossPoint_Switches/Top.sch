<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="o_sfp_tx_fault" />
        <signal name="ol_sfp_los" />
        <signal name="ol_sfp_mod_def0" />
        <signal name="cl_sfp_los" />
        <signal name="cl_sfp_mod_def0" />
        <signal name="s_sfp_los" />
        <signal name="s_sfp_mod_def0" />
        <signal name="o_sfp_los" />
        <signal name="o_sfp_mod_def0" />
        <signal name="lvcmos_wr_sel" />
        <signal name="clk_20mhz" />
        <signal name="XLXN_15" />
        <signal name="s_sfp_tx_fault" />
        <signal name="cl_sfp_tx_fault" />
        <signal name="ol_sfp_tx_fault" />
        <signal name="ol_sfp_ok" />
        <signal name="ol_sfp_fail" />
        <signal name="ol_sfp_tx_disable" />
        <signal name="ol_sfp_rate_sel" />
        <signal name="ol_sfp_mod_def1" />
        <signal name="ol_sfp_mod_def2" />
        <signal name="cl_sfp_ok" />
        <signal name="cl_sfp_fail" />
        <signal name="cl_sfp_tx_disable" />
        <signal name="cl_sfp_rate_sel" />
        <signal name="cl_sfp_mod_def1" />
        <signal name="cl_sfp_mod_def2" />
        <signal name="s_sfp_ok" />
        <signal name="s_sfp_fail" />
        <signal name="s_sfp_tx_disable" />
        <signal name="s_sfp_rate_sel" />
        <signal name="s_sfp_mod_def1" />
        <signal name="s_sfp_mod_def2" />
        <signal name="o_sfp_ok" />
        <signal name="o_sfp_fail" />
        <signal name="o_sfp_tx_disable" />
        <signal name="o_sfp_rate_sel" />
        <signal name="o_sfp_mod_def1" />
        <signal name="o_sfp_mod_def2" />
        <signal name="o_s_led" />
        <signal name="lvcmos_sel1" />
        <signal name="lvcmos_sel0" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <port polarity="Input" name="o_sfp_tx_fault" />
        <port polarity="Input" name="ol_sfp_los" />
        <port polarity="Input" name="ol_sfp_mod_def0" />
        <port polarity="Input" name="cl_sfp_los" />
        <port polarity="Input" name="cl_sfp_mod_def0" />
        <port polarity="Input" name="s_sfp_los" />
        <port polarity="Input" name="s_sfp_mod_def0" />
        <port polarity="Input" name="o_sfp_los" />
        <port polarity="Input" name="o_sfp_mod_def0" />
        <port polarity="Input" name="lvcmos_wr_sel" />
        <port polarity="Input" name="clk_20mhz" />
        <port polarity="Input" name="s_sfp_tx_fault" />
        <port polarity="Input" name="cl_sfp_tx_fault" />
        <port polarity="Input" name="ol_sfp_tx_fault" />
        <port polarity="Output" name="ol_sfp_ok" />
        <port polarity="Output" name="ol_sfp_fail" />
        <port polarity="Output" name="ol_sfp_tx_disable" />
        <port polarity="Output" name="ol_sfp_rate_sel" />
        <port polarity="Output" name="ol_sfp_mod_def1" />
        <port polarity="BiDirectional" name="ol_sfp_mod_def2" />
        <port polarity="Output" name="cl_sfp_ok" />
        <port polarity="Output" name="cl_sfp_fail" />
        <port polarity="Output" name="cl_sfp_tx_disable" />
        <port polarity="Output" name="cl_sfp_rate_sel" />
        <port polarity="Output" name="cl_sfp_mod_def1" />
        <port polarity="BiDirectional" name="cl_sfp_mod_def2" />
        <port polarity="Output" name="s_sfp_ok" />
        <port polarity="Output" name="s_sfp_fail" />
        <port polarity="Output" name="s_sfp_tx_disable" />
        <port polarity="Output" name="s_sfp_rate_sel" />
        <port polarity="Output" name="s_sfp_mod_def1" />
        <port polarity="BiDirectional" name="s_sfp_mod_def2" />
        <port polarity="Output" name="o_sfp_ok" />
        <port polarity="Output" name="o_sfp_fail" />
        <port polarity="Output" name="o_sfp_tx_disable" />
        <port polarity="Output" name="o_sfp_rate_sel" />
        <port polarity="Output" name="o_sfp_mod_def1" />
        <port polarity="BiDirectional" name="o_sfp_mod_def2" />
        <port polarity="Output" name="o_s_led" />
        <port polarity="Output" name="lvcmos_sel1" />
        <port polarity="Output" name="lvcmos_sel0" />
        <blockdef name="SFP_Config">
            <timestamp>2013-7-3T12:29:40</timestamp>
            <rect style="fillcolor:rgb(192,220,192);fillstyle:Solid" width="400" x="64" y="-1088" height="1088" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="80" y1="-48" y2="-32" x1="64" />
            <line x2="80" y1="-16" y2="-32" x1="64" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="528" y1="-128" y2="-128" x1="464" />
            <line x2="528" y1="-96" y2="-96" x1="464" />
            <line x2="528" y1="-160" y2="-160" x1="464" />
            <line x2="528" y1="-224" y2="-224" x1="464" />
            <line x2="528" y1="-256" y2="-256" x1="464" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="528" y1="-288" y2="-288" x1="464" />
            <line x2="528" y1="-320" y2="-320" x1="464" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="528" y1="-448" y2="-448" x1="464" />
            <line x2="528" y1="-480" y2="-480" x1="464" />
            <line x2="528" y1="-512" y2="-512" x1="464" />
            <line x2="528" y1="-544" y2="-544" x1="464" />
            <line x2="0" y1="-448" y2="-448" x1="64" />
            <line x2="0" y1="-512" y2="-512" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="528" y1="-352" y2="-352" x1="464" />
            <line x2="528" y1="-384" y2="-384" x1="464" />
            <line x2="528" y1="-608" y2="-608" x1="464" />
            <line x2="528" y1="-576" y2="-576" x1="464" />
            <line x2="528" y1="-672" y2="-672" x1="464" />
            <line x2="528" y1="-704" y2="-704" x1="464" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="528" y1="-736" y2="-736" x1="464" />
            <line x2="528" y1="-768" y2="-768" x1="464" />
            <line x2="0" y1="-768" y2="-768" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="528" y1="-832" y2="-832" x1="464" />
            <line x2="528" y1="-800" y2="-800" x1="464" />
            <line x2="528" y1="-896" y2="-896" x1="464" />
            <line x2="528" y1="-928" y2="-928" x1="464" />
            <line x2="0" y1="-896" y2="-896" x1="64" />
            <line x2="528" y1="-960" y2="-960" x1="464" />
            <line x2="528" y1="-992" y2="-992" x1="464" />
            <line x2="0" y1="-992" y2="-992" x1="64" />
            <line x2="0" y1="-960" y2="-960" x1="64" />
            <line x2="528" y1="-1056" y2="-1056" x1="464" />
            <line x2="528" y1="-1024" y2="-1024" x1="464" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="ibufg">
            <timestamp>2009-3-20T10:10:10</timestamp>
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <block symbolname="SFP_Config" name="XLXI_1">
            <blockpin signalname="XLXN_44" name="clk_20mhz" />
            <blockpin signalname="XLXN_15" name="reset" />
            <blockpin signalname="lvcmos_wr_sel" name="lvcmos_wr_sel" />
            <blockpin signalname="lvcmos_sel1" name="lvcmos_sel1" />
            <blockpin signalname="lvcmos_sel0" name="lvcmos_sel0" />
            <blockpin signalname="o_s_led" name="o_s_led" />
            <blockpin signalname="o_sfp_mod_def2" name="o_sfp_mod_def2" />
            <blockpin signalname="o_sfp_mod_def1" name="o_sfp_mod_def1" />
            <blockpin signalname="o_sfp_mod_def0" name="o_sfp_mod_def0" />
            <blockpin signalname="o_sfp_rate_sel" name="o_sfp_rate_sel" />
            <blockpin signalname="o_sfp_tx_disable" name="o_sfp_tx_disable" />
            <blockpin signalname="o_sfp_tx_fault" name="o_sfp_tx_fault" />
            <blockpin signalname="o_sfp_los" name="o_sfp_los" />
            <blockpin signalname="s_sfp_mod_def2" name="s_sfp_mod_def2" />
            <blockpin signalname="s_sfp_mod_def1" name="s_sfp_mod_def1" />
            <blockpin signalname="s_sfp_rate_sel" name="s_sfp_rate_sel" />
            <blockpin signalname="s_sfp_tx_disable" name="s_sfp_tx_disable" />
            <blockpin signalname="s_sfp_mod_def0" name="s_sfp_mod_def0" />
            <blockpin signalname="s_sfp_los" name="s_sfp_los" />
            <blockpin signalname="s_sfp_tx_fault" name="s_sfp_tx_fault" />
            <blockpin signalname="o_sfp_fail" name="o_sfp_fail" />
            <blockpin signalname="o_sfp_ok" name="o_sfp_ok" />
            <blockpin signalname="s_sfp_ok" name="s_sfp_ok" />
            <blockpin signalname="s_sfp_fail" name="s_sfp_fail" />
            <blockpin signalname="cl_sfp_mod_def2" name="cl_sfp_mod_def2" />
            <blockpin signalname="cl_sfp_mod_def1" name="cl_sfp_mod_def1" />
            <blockpin signalname="cl_sfp_mod_def0" name="cl_sfp_mod_def0" />
            <blockpin signalname="cl_sfp_rate_sel" name="cl_sfp_rate_sel" />
            <blockpin signalname="cl_sfp_tx_disable" name="cl_sfp_tx_disable" />
            <blockpin signalname="cl_sfp_tx_fault" name="cl_sfp_tx_fault" />
            <blockpin signalname="cl_sfp_los" name="cl_sfp_los" />
            <blockpin signalname="cl_sfp_ok" name="cl_sfp_ok" />
            <blockpin signalname="cl_sfp_fail" name="cl_sfp_fail" />
            <blockpin signalname="ol_sfp_mod_def2" name="ol_sfp_mod_def2" />
            <blockpin signalname="ol_sfp_mod_def1" name="ol_sfp_mod_def1" />
            <blockpin signalname="ol_sfp_mod_def0" name="ol_sfp_mod_def0" />
            <blockpin signalname="ol_sfp_rate_sel" name="ol_sfp_rate_sel" />
            <blockpin signalname="ol_sfp_tx_disable" name="ol_sfp_tx_disable" />
            <blockpin signalname="ol_sfp_tx_fault" name="ol_sfp_tx_fault" />
            <blockpin signalname="ol_sfp_los" name="ol_sfp_los" />
            <blockpin signalname="ol_sfp_ok" name="ol_sfp_ok" />
            <blockpin signalname="ol_sfp_fail" name="ol_sfp_fail" />
        </block>
        <block symbolname="gnd" name="XLXI_2">
            <blockpin signalname="XLXN_15" name="G" />
        </block>
        <block symbolname="ibufg" name="XLXI_3">
            <blockpin signalname="clk_20mhz" name="I" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="1344" height="1900">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="432" y="1424" name="XLXI_1" orien="R0">
        </instance>
        <branch name="ol_sfp_tx_fault">
            <wire x2="432" y1="432" y2="432" x1="304" />
        </branch>
        <branch name="ol_sfp_los">
            <wire x2="432" y1="464" y2="464" x1="304" />
        </branch>
        <branch name="ol_sfp_mod_def0">
            <wire x2="432" y1="528" y2="528" x1="304" />
        </branch>
        <branch name="cl_sfp_tx_fault">
            <wire x2="432" y1="656" y2="656" x1="304" />
        </branch>
        <branch name="cl_sfp_los">
            <wire x2="432" y1="688" y2="688" x1="304" />
        </branch>
        <branch name="cl_sfp_mod_def0">
            <wire x2="432" y1="752" y2="752" x1="304" />
        </branch>
        <branch name="s_sfp_tx_fault">
            <wire x2="432" y1="880" y2="880" x1="304" />
        </branch>
        <branch name="s_sfp_los">
            <wire x2="432" y1="912" y2="912" x1="304" />
        </branch>
        <branch name="s_sfp_mod_def0">
            <wire x2="432" y1="976" y2="976" x1="304" />
        </branch>
        <branch name="o_sfp_tx_fault">
            <wire x2="432" y1="1104" y2="1104" x1="304" />
        </branch>
        <branch name="o_sfp_los">
            <wire x2="432" y1="1136" y2="1136" x1="304" />
        </branch>
        <branch name="o_sfp_mod_def0">
            <wire x2="432" y1="1200" y2="1200" x1="304" />
        </branch>
        <branch name="lvcmos_wr_sel">
            <wire x2="432" y1="1328" y2="1328" x1="304" />
        </branch>
        <iomarker fontsize="28" x="304" y="432" name="ol_sfp_tx_fault" orien="R180" />
        <iomarker fontsize="28" x="304" y="464" name="ol_sfp_los" orien="R180" />
        <iomarker fontsize="28" x="304" y="528" name="ol_sfp_mod_def0" orien="R180" />
        <iomarker fontsize="28" x="304" y="656" name="cl_sfp_tx_fault" orien="R180" />
        <iomarker fontsize="28" x="304" y="688" name="cl_sfp_los" orien="R180" />
        <iomarker fontsize="28" x="304" y="752" name="cl_sfp_mod_def0" orien="R180" />
        <iomarker fontsize="28" x="304" y="880" name="s_sfp_tx_fault" orien="R180" />
        <iomarker fontsize="28" x="304" y="912" name="s_sfp_los" orien="R180" />
        <iomarker fontsize="28" x="304" y="976" name="s_sfp_mod_def0" orien="R180" />
        <iomarker fontsize="28" x="304" y="1104" name="o_sfp_tx_fault" orien="R180" />
        <iomarker fontsize="28" x="304" y="1136" name="o_sfp_los" orien="R180" />
        <iomarker fontsize="28" x="304" y="1200" name="o_sfp_mod_def0" orien="R180" />
        <branch name="ol_sfp_ok">
            <wire x2="1040" y1="368" y2="368" x1="960" />
        </branch>
        <branch name="ol_sfp_fail">
            <wire x2="1040" y1="400" y2="400" x1="960" />
        </branch>
        <branch name="ol_sfp_tx_disable">
            <wire x2="1040" y1="432" y2="432" x1="960" />
        </branch>
        <branch name="ol_sfp_rate_sel">
            <wire x2="1040" y1="464" y2="464" x1="960" />
        </branch>
        <branch name="ol_sfp_mod_def1">
            <wire x2="1040" y1="496" y2="496" x1="960" />
        </branch>
        <branch name="ol_sfp_mod_def2">
            <wire x2="1040" y1="528" y2="528" x1="960" />
        </branch>
        <branch name="cl_sfp_ok">
            <wire x2="1040" y1="592" y2="592" x1="960" />
        </branch>
        <branch name="cl_sfp_fail">
            <wire x2="1040" y1="624" y2="624" x1="960" />
        </branch>
        <branch name="cl_sfp_tx_disable">
            <wire x2="1040" y1="656" y2="656" x1="960" />
        </branch>
        <branch name="cl_sfp_rate_sel">
            <wire x2="1040" y1="688" y2="688" x1="960" />
        </branch>
        <branch name="cl_sfp_mod_def1">
            <wire x2="1040" y1="720" y2="720" x1="960" />
        </branch>
        <branch name="cl_sfp_mod_def2">
            <wire x2="1040" y1="752" y2="752" x1="960" />
        </branch>
        <branch name="s_sfp_ok">
            <wire x2="1040" y1="816" y2="816" x1="960" />
        </branch>
        <branch name="s_sfp_fail">
            <wire x2="1040" y1="848" y2="848" x1="960" />
        </branch>
        <branch name="s_sfp_tx_disable">
            <wire x2="1040" y1="880" y2="880" x1="960" />
        </branch>
        <branch name="s_sfp_rate_sel">
            <wire x2="1040" y1="912" y2="912" x1="960" />
        </branch>
        <branch name="s_sfp_mod_def1">
            <wire x2="1040" y1="944" y2="944" x1="960" />
        </branch>
        <branch name="s_sfp_mod_def2">
            <wire x2="1040" y1="976" y2="976" x1="960" />
        </branch>
        <branch name="o_sfp_ok">
            <wire x2="1040" y1="1040" y2="1040" x1="960" />
        </branch>
        <branch name="o_sfp_fail">
            <wire x2="1040" y1="1072" y2="1072" x1="960" />
        </branch>
        <branch name="o_sfp_tx_disable">
            <wire x2="1040" y1="1104" y2="1104" x1="960" />
        </branch>
        <branch name="o_sfp_rate_sel">
            <wire x2="1040" y1="1136" y2="1136" x1="960" />
        </branch>
        <branch name="o_sfp_mod_def1">
            <wire x2="1040" y1="1168" y2="1168" x1="960" />
        </branch>
        <branch name="o_sfp_mod_def2">
            <wire x2="1040" y1="1200" y2="1200" x1="960" />
        </branch>
        <branch name="o_s_led">
            <wire x2="1040" y1="1264" y2="1264" x1="960" />
        </branch>
        <branch name="lvcmos_sel1">
            <wire x2="1040" y1="1296" y2="1296" x1="960" />
        </branch>
        <branch name="lvcmos_sel0">
            <wire x2="1040" y1="1328" y2="1328" x1="960" />
        </branch>
        <iomarker fontsize="28" x="304" y="1328" name="lvcmos_wr_sel" orien="R180" />
        <iomarker fontsize="28" x="1040" y="368" name="ol_sfp_ok" orien="R0" />
        <iomarker fontsize="28" x="1040" y="400" name="ol_sfp_fail" orien="R0" />
        <iomarker fontsize="28" x="1040" y="432" name="ol_sfp_tx_disable" orien="R0" />
        <iomarker fontsize="28" x="1040" y="464" name="ol_sfp_rate_sel" orien="R0" />
        <iomarker fontsize="28" x="1040" y="496" name="ol_sfp_mod_def1" orien="R0" />
        <iomarker fontsize="28" x="1040" y="592" name="cl_sfp_ok" orien="R0" />
        <iomarker fontsize="28" x="1040" y="624" name="cl_sfp_fail" orien="R0" />
        <iomarker fontsize="28" x="1040" y="656" name="cl_sfp_tx_disable" orien="R0" />
        <iomarker fontsize="28" x="1040" y="688" name="cl_sfp_rate_sel" orien="R0" />
        <iomarker fontsize="28" x="1040" y="720" name="cl_sfp_mod_def1" orien="R0" />
        <iomarker fontsize="28" x="1040" y="816" name="s_sfp_ok" orien="R0" />
        <iomarker fontsize="28" x="1040" y="848" name="s_sfp_fail" orien="R0" />
        <iomarker fontsize="28" x="1040" y="880" name="s_sfp_tx_disable" orien="R0" />
        <iomarker fontsize="28" x="1040" y="912" name="s_sfp_rate_sel" orien="R0" />
        <iomarker fontsize="28" x="1040" y="944" name="s_sfp_mod_def1" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1040" name="o_sfp_ok" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1072" name="o_sfp_fail" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1104" name="o_sfp_tx_disable" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1136" name="o_sfp_rate_sel" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1168" name="o_sfp_mod_def1" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1264" name="o_s_led" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1296" name="lvcmos_sel1" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1328" name="lvcmos_sel0" orien="R0" />
        <iomarker fontsize="28" x="1040" y="528" name="ol_sfp_mod_def2" orien="R0" />
        <iomarker fontsize="28" x="1040" y="752" name="cl_sfp_mod_def2" orien="R0" />
        <iomarker fontsize="28" x="1040" y="976" name="s_sfp_mod_def2" orien="R0" />
        <iomarker fontsize="28" x="1040" y="1200" name="o_sfp_mod_def2" orien="R0" />
        <instance x="688" y="1552" name="XLXI_3" orien="M0" />
        <branch name="clk_20mhz">
            <wire x2="704" y1="1520" y2="1520" x1="688" />
            <wire x2="1040" y1="1520" y2="1520" x1="704" />
        </branch>
        <iomarker fontsize="28" x="1040" y="1520" name="clk_20mhz" orien="R0" />
        <branch name="XLXN_44">
            <wire x2="432" y1="1392" y2="1392" x1="368" />
            <wire x2="368" y1="1392" y2="1520" x1="368" />
            <wire x2="464" y1="1520" y2="1520" x1="368" />
        </branch>
        <instance x="64" y="1328" name="XLXI_2" orien="R90" />
        <branch name="XLXN_15">
            <wire x2="320" y1="1392" y2="1392" x1="192" />
            <wire x2="320" y1="1360" y2="1392" x1="320" />
            <wire x2="432" y1="1360" y2="1360" x1="320" />
        </branch>
    </sheet>
</drawing>